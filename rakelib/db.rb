# Copyright (C) 2020 Thomas Baron
#
# This file is part of dopewars.
#
# dopewars is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3 of the License.
#
# dopewars is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with dopewars.  If not, see <https://www.gnu.org/licenses/>.
def create_schema
  ActiveRecord::Schema.define do
    # expiration
    create_table :expiration do |t|
      t.integer :created_at
      t.integer :updated_at
      t.integer :expired_at
      t.string :object_name
      t.integer :object_id
    end
    # source_ip
    create_table :source_ip do |t|
      t.integer :created_at
      t.integer :updated_at
      t.string :address, null: false
      t.integer :player_creation_count
    end
    add_index :source_ip, :address, unique: true
    # player
    create_table :player do |t|
      t.integer :created_at
      t.integer :updated_at
      t.string :public_id, null: false
      t.string :name, null: false
      t.string :pass
      t.string :salt
    end
    add_index :player, :public_id, unique: true
    add_index :player, :name, unique: true
    # game
    create_table :game do |t|
      t.integer :created_at
      t.integer :updated_at
      t.string :public_id, null: false
      t.integer :active
      t.integer :standard
      t.integer :score
      t.integer :cash
      t.integer :bank
      t.integer :debt
      t.integer :stock
      t.integer :crew
      t.string :config
      t.integer :player_id
    end
    add_index :game, :public_id, unique: true
    add_foreign_key :game, :player, column: :player_id
    add_index :game, :player_id
    # play
    create_table :play do |t|
      t.integer :created_at
      t.integer :updated_at
      t.integer :turn
      t.integer :location_kind
      t.integer :health
      t.integer :sidekicks
      t.integer :sidekick_comp
      t.integer :game_id
    end
    add_foreign_key :play, :game, column: :game_id
    add_index :play, :game_id
    # business
    create_table :business do |t|
      t.integer :created_at
      t.integer :updated_at
      t.integer :drug_kind
      t.integer :buy_quantity
      t.integer :buy_value
      t.integer :sell_quantity
      t.integer :sell_value
      t.integer :game_id
    end
    add_foreign_key :business, :game, column: :game_id
    add_index :business, :game_id
    # event
    create_table :event do |t|
      t.integer :created_at
      t.integer :updated_at
      t.string :description
      t.integer :kind
      t.integer :location_kind
      t.integer :turn
      t.integer :game_id
    end
    add_foreign_key :event, :game, column: :game_id
    add_index :event, :game_id
    # inventory_item
    create_table :inventory_item do |t|
      t.integer :created_at
      t.integer :updated_at
      t.integer :drug_kind
      t.integer :quantity
      t.integer :value
      t.integer :game_id
    end
    add_foreign_key :inventory_item, :game, column: :game_id
    add_index :inventory_item, :game_id
    # location
    create_table :location do |t|
      t.integer :created_at
      t.integer :updated_at
      t.integer :kind
      t.integer :threat
      t.integer :game_id
    end
    add_foreign_key :location, :game, column: :game_id
    add_index :location, :game_id
    # offer
    create_table :offer do |t|
      t.integer :created_at
      t.integer :updated_at
      t.integer :drug_kind
      t.integer :unit_price
      t.integer :game_id
    end
    add_foreign_key :offer, :game, column: :game_id
    add_index :offer, :game_id
  end
end
