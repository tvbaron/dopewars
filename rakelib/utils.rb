# Copyright (C) 2020 Thomas Baron
#
# This file is part of dopewars.
#
# dopewars is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3 of the License.
#
# dopewars is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with dopewars.  If not, see <https://www.gnu.org/licenses/>.
class Randomizer
  ALPHANUM = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz".freeze
  ALPHANUM_UPCASE = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ".freeze
  def initialize
    @prng = Random.new(Time.now.to_i)
  end
  # Returns a random integer.
  # @param max [Integer] Maximum value, exclusive.
  # @return [Integer]
  def random_number(max)
    @prng.rand(max)
  end
  # Creates a new random string.
  # @param opts [Hash]
  # @return [String]
  def generate_string(opts = {})
    len = opts.fetch(:length, 8)
    alphabet = opts.fetch(:alphabet, ALPHANUM)
    sb = StringIO.new
    len.times do
      sb << alphabet[random_number(alphabet.length)]
    end
    sb.string
  end
end
$randomizer = Randomizer.new
# Returns a random integer.
# @param min [Integer] Mininum value, inclusive.
# @param max [Integer] Maximum value, inclusive.
# @return [Integer]
def random_number(min, max)
  $randomizer.random_number(max - min + 1) + min
end
# Creates a new hash
# @param opts [Hash]
# @option opts [Integer] :length
# @option opts [String] :alphabet
# @return [String]
def generate_hash(opts = {})
  len = opts.fetch(:length, 32)
  alphabet = opts.fetch(:alphabet, HASH_ALPHABET)
  sb = StringIO.new
  len.times do
    sb << alphabet[$randomizer.random_number(alphabet.length)]
  end
  sb.string
end
def hexdigest(*values)
  sha = Digest::SHA256.new
  values.each { |v| sha.update v }
  sha.hexdigest
end
