# Copyright (C) 2020 Thomas Baron
#
# This file is part of dopewars.
#
# dopewars is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3 of the License.
#
# dopewars is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with dopewars.  If not, see <https://www.gnu.org/licenses/>.
require_relative 'utils'
require_relative 'db'
namespace :db do
  desc 'Creates the database and the schema'
  task :create do
    config = $config.dup
    database = config.delete 'database'
    ActiveRecord::Base.establish_connection(config)
    ActiveRecord::Base.connection.create_database database, config
    ActiveRecord::Base.connection.close
    ActiveRecord::Base.establish_connection($config)
    create_schema
    ActiveRecord::Base.connection.close
  end
  desc 'Drops the database'
  task :drop do
    config = $config.dup
    database = config.delete 'database'
    ActiveRecord::Base.establish_connection(config)
    ActiveRecord::Base.connection.drop_database database
    ActiveRecord::Base.connection.close
  end
end
