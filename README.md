# dopewars

Web impl. of dopewars.

Live demo at [yellowcube.net](https://yellowcube.net/dopewars)

## Deployment

### Start postgresql service

With docker.

    docker run -d -e POSTGRES_PASSWORD=f84gdiowvdow5bvb -v $(realpath .)/data:/var/lib/postgresql/data --publish 5432:5432 postgres:12-alpine

### Create _.token_key_

    openssl rand -base64 2048 > .token_key

### Start app

    rackup

Open your browser to http://localhost:9292
