# Changelog for dopewars

## 1.2.0

### Changed

- Disabled logging in production.

### Added

- Added rate limit on player creation.
- Added rate limit on game creation.
- Added game hash validation.
- Added transactions.

## 1.1.0

### Fixed

- Fixed quantity validation for selling.

### Changed

- Switched database from sqlite3 to postgresql.
- Moved handle_expiration to utils.
- Changed _.token_key_ format to base64.
- Changed wall of fame, made it available for unlogged in users.
- Changed about the game, made it available for unlogged in users.

### Removed

- Removed authentication middleware. Using simple helper instead (more flexible).

## 1.0.0 - 2020-01-11

### Added

- Initial base code.
