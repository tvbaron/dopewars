# Dopewars - Deployment

## Prerequisite

**postgresql** is installed with docker and started with the following command:

    docker run -d -e POSTGRES_PASSWORD=production_password -v $(realpath .)/data:/var/lib/postgresql/data --publish 127.0.0.1:5432:5432 postgres:12-alpine

Replace _production_password_ both in command line and _config/database.yaml_.

**ruby** is installed in __/usr/local/bin__.

**dopewars** in installed in __/opt__.

## Installation

### Create database and schema

    APP_ENV=production rake db:create

### Create _.token_key_ file

    openssl rand -base64 2048 > .token_key

### Install cron file

    cp deployment/cronfile /etc/cron.d/dopewars

### Install systemd service

    cp deployment/service /etc/systemd/system/dopewars.service

## Start application

    systemctl start dopewars
