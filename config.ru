# Copyright (C) 2020 Thomas Baron
#
# This file is part of dopewars.
#
# dopewars is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3 of the License.
#
# dopewars is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with dopewars.  If not, see <https://www.gnu.org/licenses/>.
# web
require 'sinatra/base'
require 'sinatra/content_for'
# database
require 'pg'
require 'active_record'
# other
require 'digest'
require 'jwt'
require 'yaml'
# models
require_relative 'app/models/expiration'
require_relative 'app/models/source_ip'
require_relative 'app/models/player'
require_relative 'app/models/game'
require_relative 'app/models/play'
require_relative 'app/models/business'
require_relative 'app/models/location'
require_relative 'app/models/offer'
require_relative 'app/models/inventory_item'
require_relative 'app/models/event'
# connect to database
db_config = YAML.load_file 'config/database.yaml'
db_config = db_config.fetch ENV.fetch('APP_ENV', 'development')
ActiveRecord::Base.establish_connection(db_config)
# helpers
require_relative 'app/helpers/application_helper'
require_relative 'app/helpers/session_helper'
require_relative 'app/helpers/player_helper'
require_relative 'app/helpers/game_helper'
# controllers
require_relative 'app/controllers/application_controller'
require_relative 'app/controllers/api/player_controller'
require_relative 'app/controllers/api/game_controller'
require_relative 'app/controllers/web/player_controller'
require_relative 'app/controllers/web/game_controller'
require_relative 'app/controllers/web/home_controller'
# environment
ENV['BASE_URL'] ||= 'https://yellowcube.tom/dopewars'
ENV['APP_NAME'] ||= 'dopewars'
# routes
map('/dopewars/api/player') { run Api::PlayerController }
map('/dopewars/api/game') { run Api::GameController }
map('/dopewars/player') { run Web::PlayerController }
map('/dopewars/game') { run Web::GameController }
map('/dopewars') { run Web::HomeController }
