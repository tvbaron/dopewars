# Copyright (C) 2020 Thomas Baron
#
# This file is part of dopewars.
#
# dopewars is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3 of the License.
#
# dopewars is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with dopewars.  If not, see <https://www.gnu.org/licenses/>.
# database
require 'pg'
require 'active_record'
require_relative 'lib/utils'
# models
require_relative '../app/models/player'
require_relative '../app/models/game'
require_relative '../app/models/play'
require_relative '../app/models/business'
require_relative '../app/models/location'
require_relative '../app/models/offer'
require_relative '../app/models/inventory_item'
require_relative '../app/models/event'
require_relative '../app/models/expiration'
DrugSpec = Struct.new("DrugSpec", :kind, :name, :min, :max, :cheap, :expensive)
drug_specs = []
h = YAML.load_file File.join(File.dirname(__FILE__), '../app/helpers/drugs.yaml')
h["drugs"].each_with_index do |d, i|
  drug_specs << DrugSpec.new(i + 1, d['name'], d['min'].to_i, d['max'].to_i, d['cheap'], d['expensive'])
end
db_config = YAML.load_file File.join(File.dirname(__FILE__), '../config/database.yaml')
db_config = db_config.fetch 'development'
game_hash = nil
args = ARGV.dup
until args.empty?
  arg = args.shift
  if arg == '-g'
    game_hash = String(args.shift)
  end
end
unless game_hash
  Utils.log :error, 'missing game_hash (-g)'
  exit 1
end
commit = (ENV['COMMIT'] == '1')
if commit
  Utils.log :notice, 'commit mode'
else
  Utils.log :notice, 'dry run mode'
end
ActiveRecord::Base.establish_connection(db_config)
game = Game.where("public_id = '#{game_hash}'").take
unless game
  Utils.log :error, 'no such game'
  exit 1
end
exit 0 unless commit
play = game.play
items = game.inventory_items.to_a
Utils.log :info, "sidekicks: #{play.sidekicks} + 1"
play.sidekicks += 1
Utils.log :info, "health: #{play.health} + 100"
play.health += 100
play.save
total_space = 10 + (play.sidekicks * 10)
occupied_space = InventoryItem.where("game_id = #{game.id}").sum("quantity")
available_space = total_space - occupied_space
Utils.log :info, "available_space: #{available_space}"
item_quantity = available_space / items.length
if item_quantity > 0
  items.each do |item|
    drug_spec = drug_specs.find { |s| s.kind == item.drug_kind }
    Utils.log :info, "(#{drug_spec.name}) quantity: #{item.quantity} + #{item_quantity}"
    item.quantity += item_quantity
    item.save
  end
end
ActiveRecord::Base.connection.close
