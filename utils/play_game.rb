# Copyright (C) 2020 Thomas Baron
#
# This file is part of dopewars.
#
# dopewars is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3 of the License.
#
# dopewars is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with dopewars.  If not, see <https://www.gnu.org/licenses/>.
require 'faraday'
require 'json'
require 'uri'
require_relative 'lib/utils'
require_relative 'lib/api'
require_relative 'lib/context'
BASE_URL = 'https://yellowcube.tom'
VERIFY_SSL = false
DEBUG = false
PLAYER_NAME = 'Guest'
PLAYER_PASSWORD = 'Guest'
ctx = Context.new({:base_url => BASE_URL, :verify_ssl => VERIFY_SSL, :debug => DEBUG})
ctx.authenticate_player(PLAYER_NAME, PLAYER_PASSWORD)
config = ctx.fetch_config
def fetch_config_drug(config, name)
  config['drugs'].find { |d| d['name'] == name }
end
ctx.create_game
ctx.fetch_inventory
while ctx.game_inventory['active']
  turn = ctx.game_inventory['turn']
  location_name = ctx.game_inventory['current_location']['name']
  # sell
  ctx.game_inventory['items'].each do |item|
    offer = ctx.game_inventory['offers'].find { |o| o['drug_name'] == item['drug_name'] }
    next unless offer
    profit = (offer['unit_price'] * item['quantity']) - item['value']
    next if profit <= 0
    Utils.log :info,  "[#{turn}-#{location_name}] sell #{item['quantity']} unit(s) of #{item['drug_name']}"
    ctx.sell_drugs(item['drug_name'], item['quantity'])
  end
  Utils.log :info,  "[#{turn}-#{location_name}] cash is #{Utils.format_credits(ctx.game_inventory['cash'])}"
  # hire sidekick, buy
  if turn < config['turn_limit']
    # hire sidekick
    if ctx.game_inventory['sidekick_comp'] && (ctx.game_inventory['cash'] > ctx.game_inventory['crew'])
      Utils.log :info,  "[#{turn}-#{location_name}] hire sidekick"
      ctx.hire_sidekick
    end
    # buy
    buy_name = nil
    buy_quantity = nil
    buy_profit = nil
    ctx.game_inventory['offers'].each do |offer|
      drug_spec = fetch_config_drug(config, offer['drug_name'])
      next if offer['unit_price'] >= ((drug_spec['max_value'] + drug_spec['min_value']) / 2)
      quantity = ctx.game_inventory['cash'] / offer['unit_price']
      quantity = ctx.game_inventory['space'] if quantity > ctx.game_inventory['space']
      if quantity > 0
        profit = (drug_spec['max_value'] - offer['unit_price']) * quantity
        if (buy_name == nil) || (buy_profit < profit)
          buy_name = offer['drug_name']
          buy_quantity = quantity
          buy_profit = profit
        end
      end
    end
    if buy_name
      Utils.log :info,  "[#{turn}-#{location_name}] buy #{buy_quantity} unit(s) of #{buy_name}"
      ctx.buy_drugs(buy_name, buy_quantity)
    end
  end
  # jet
  jet_name = nil
  jet_threat = nil
  ctx.game_inventory['locations'].each do |location|
    next if location['name'] == location_name
    threat = location['threat']
    if (jet_name == nil) || (jet_threat > threat)
      jet_name = location['name']
      jet_threat = threat
    end
  end
  Utils.log :info,  "[#{turn}-#{location_name}] jet to #{jet_name}"
  ctx.change_location(jet_name)
end
Utils.log :info,  "score is #{Utils.format_credits(ctx.game_inventory['score'])}"
