# Copyright (C) 2020 Thomas Baron
#
# This file is part of dopewars.
#
# dopewars is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3 of the License.
#
# dopewars is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with dopewars.  If not, see <https://www.gnu.org/licenses/>.
# database
require 'pg'
require 'active_record'
require_relative 'lib/utils'
# models
require_relative '../app/models/expiration'
require_relative '../app/models/source_ip'
require_relative '../app/models/player'
require_relative '../app/models/game'
require_relative '../app/models/play'
require_relative '../app/models/business'
require_relative '../app/models/location'
require_relative '../app/models/offer'
require_relative '../app/models/inventory_item'
require_relative '../app/models/event'
db_config = YAML.load_file File.join(File.dirname(__FILE__), '../config/database.yaml')
db_config = db_config.fetch ENV.fetch('APP_ENV', 'development')
commit = (ENV['COMMIT'] == '1')
if commit
  Utils.log :notice, 'commit mode'
else
  Utils.log :notice, 'dry run mode'
end
ActiveRecord::Base.establish_connection(db_config)
now = Time.now.tv_sec
Expiration.where(['expired_at <= ?', now]).to_a.each do |e|
  Utils.log :info, "handle expiration #{e.id} -> #{e.object_name} #{e.object_id}"
  catch :abort do
    if e.object_name == "game"
      game = Game.where("id = #{e.object_id}").take
      unless game
        Utils.log :error, 'no such game #{e.object_id}'
        throw :abort
      end
      if game.active == 0
        Utils.log :info, "game #{game.id} is terminated"
        throw :abort
      end
      Utils.log :notice, "destroy game #{game.id}"
      if commit
        ActiveRecord::Base.transaction do
          Business.where("game_id = #{game.id}").destroy_all
          Event.where("game_id = #{game.id}").destroy_all
          InventoryItem.where("game_id = #{game.id}").destroy_all
          Location.where("game_id = #{game.id}").destroy_all
          Offer.where("game_id = #{game.id}").destroy_all
          Play.where("game_id = #{game.id}").destroy_all
          game.destroy
        end # transaction
      end
    end
  end
  Utils.log :notice, "destroy expiration #{e.id}"
  e.destroy if commit
end
yesterday = now - 24*60*60
SourceIP.where(['updated_at <= ?', yesterday]).to_a.each do |s|
  Utils.log :info, "handle source_id #{s.id} -> #{s.address} #{s.player_creation_count}"
  next unless commit
  if s.player_creation_count > 10
    s.player_creation_count -= 10
    s.save
  else
    s.destroy
  end
end
ActiveRecord::Base.connection.close
