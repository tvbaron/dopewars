# Copyright (C) 2020 Thomas Baron
#
# This file is part of dopewars.
#
# dopewars is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3 of the License.
#
# dopewars is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with dopewars.  If not, see <https://www.gnu.org/licenses/>.
class Context
  attr_reader :token
  attr_reader :config
  attr_reader :game_hash
  attr_reader :game_inventory
  def initialize(opts)
    @client = API.new(opts)
    @token = nil
    @config = nil
    @game_hash = nil
  end
  def create_player(name, password)
    res = @client.create_player(nil, {:name => name, :password => password})
    @token = res['token']
  end
  def authenticate_player(name, password)
    res = @client.authenticate_player(nil, {:name => name, :password => password})
    @token = res['token']
  end
  def fetch_config
    @config = @client.fetch_config({:token => @token}, nil)
  end
  def create_game
    res = @client.create_game({:token => @token}, nil)
    @game_hash = res['hash']
  end
  def fetch_inventory
    res = @client.fetch_inventory({:token => @token}, {:game_hash => @game_hash})
    @game_inventory = res
  end
  def change_location(name)
    res = @client.change_location({:token => @token}, {:game_hash => @game_hash, :name => name})
    @game_inventory = res
  end
  def hire_sidekick
    res = @client.hire_sidekick({:token => @token}, {:game_hash => @game_hash})
    @game_inventory = res
  end
  def buy_drugs(name, quantity)
    res = @client.buy_drugs({:token => @token}, {:game_hash => @game_hash, :name => name, :quantity => quantity})
    @game_inventory = res
  end
  def sell_drugs(name, quantity)
    res = @client.sell_drugs({:token => @token}, {:game_hash => @game_hash, :name => name, :quantity => quantity})
    @game_inventory = res
  end
end
