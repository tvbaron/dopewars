# Copyright (C) 2020 Thomas Baron
#
# This file is part of dopewars.
#
# dopewars is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3 of the License.
#
# dopewars is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with dopewars.  If not, see <https://www.gnu.org/licenses/>.
module Utils
  # Logs a message.
  # @param level [Symbol]
  # @param message [String]
  def self.log(level, message)
    $stdout.puts "[#{Time.now.strftime('%Y-%m-%dT%H:%M:%S')} #{'%7s' % level.to_s}] #{message}"
  end
  # Formats a given amount of credits.
  # @param value [Integer]
  # @return [String]
  def self.format_credits(value)
    val_s = (value >= 0) ? value.to_s : (-value).to_s
    reader = StringIO.new val_s
    sb = StringIO.new
    n = 0
    while n < val_s.length
      c = (val_s.length - n) % 3
      if c == 0
        sb << "," unless sb.length == 0
        sb << reader.read(3)
        n += 3
      else
        sb << reader.read(c)
        n += c
      end
    end
    (value >= 0) ? "$#{sb.string}" : "-$#{sb.string}"
  end
end
