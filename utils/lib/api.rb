# Copyright (C) 2020 Thomas Baron
#
# This file is part of dopewars.
#
# dopewars is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3 of the License.
#
# dopewars is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with dopewars.  If not, see <https://www.gnu.org/licenses/>.
class APIError < StandardError
  def initialize(msg)
    super
  end
end
class API
  # @param opts [Hash]
  # @option opts [String] :base_url
  # @option opts [Boolean] :verify_ssl
  def initialize(opts = {})
    client_opts = {}
    client_opts[:ssl] = {}
    client_opts[:ssl][:verify] = opts.fetch(:verify_ssl, true)
    @client = Faraday.new(opts.fetch(:base_url), client_opts)
    @debug = opts.fetch(:debug, false)
  end
  def create_player(headers, params)
    res = @client.post '/dopewars/api/player/create' do |req|
      req.headers['Accept'] = 'application/json'
      req.headers['Content-Type'] = 'multipart/form-data'
      data = {}
      data['player_name'] = params.fetch(:name)
      data['player_password'] = params.fetch(:password)
      req.body = URI.encode_www_form(data)
    end
    if @debug
      puts "status: #{res.status}"
      puts "headers: #{res.headers}"
      puts "body: #{res.body}"
    end
    raise APIError, "wrong status: #{res.status}" if res.status != 200
    JSON.parse(res.body)
  end
  def authenticate_player(headers, params)
    res = @client.post '/dopewars/api/player/authenticate' do |req|
      req.headers['Accept'] = 'application/json'
      req.headers['Content-Type'] = 'multipart/form-data'
      data = {}
      data['player_name'] = params.fetch(:name)
      data['player_password'] = params.fetch(:password)
      req.body = URI.encode_www_form(data)
    end
    if @debug
      puts "status: #{res.status}"
      puts "headers: #{res.headers}"
      puts "body: #{res.body}"
    end
    raise APIError, "wrong status: #{res.status}" if res.status != 200
    JSON.parse(res.body)
  end
  def fetch_config(headers, params)
    res = @client.get '/dopewars/api/game/config' do |req|
      req.headers['Accept'] = 'application/json'
      req.headers['Authorization'] = "Bearer #{headers.fetch(:token)}"
    end
    if @debug
      puts "status: #{res.status}"
      puts "headers: #{res.headers}"
      puts "body: #{res.body}"
    end
    raise APIError, "wrong status: #{res.status}" if res.status != 200
    JSON.parse(res.body)
  end
  def create_game(headers, params)
    res = @client.post '/dopewars/api/game/create' do |req|
      req.headers['Accept'] = 'application/json'
      req.headers['Authorization'] = "Bearer #{headers.fetch(:token)}"
    end
    if @debug
      puts "status: #{res.status}"
      puts "headers: #{res.headers}"
      puts "body: #{res.body}"
    end
    raise APIError, "wrong status: #{res.status}" if res.status != 200
    JSON.parse(res.body)
  end
  def fetch_inventory(headers, params)
    res = @client.get "/dopewars/api/game/#{params[:game_hash]}/inventory" do |req|
      req.headers['Accept'] = 'application/json'
      req.headers['Authorization'] = "Bearer #{headers.fetch(:token)}"
    end
    if @debug
      puts "status: #{res.status}"
      puts "headers: #{res.headers}"
      puts "body: #{res.body}"
    end
    raise APIError, "wrong status: #{res.status}" if res.status != 200
    JSON.parse(res.body)
  end
  def change_location(headers, params)
    res = @client.post "/dopewars/api/game/#{params[:game_hash]}/jet" do |req|
      req.headers['Accept'] = 'application/json'
      req.headers['Authorization'] = "Bearer #{headers.fetch(:token)}"
      req.headers['Content-Type'] = 'multipart/form-data'
      data = {}
      data['location_name'] = params.fetch(:name)
      req.body = URI.encode_www_form(data)
    end
    if @debug
      puts "status: #{res.status}"
      puts "headers: #{res.headers}"
      puts "body: #{res.body}"
    end
    raise APIError, "wrong status: #{res.status}" if res.status != 200
    JSON.parse(res.body)
  end
  def hire_sidekick(headers, params)
    res = @client.post "/dopewars/api/game/#{params[:game_hash]}/hire" do |req|
      req.headers['Accept'] = 'application/json'
      req.headers['Authorization'] = "Bearer #{headers.fetch(:token)}"
      req.headers['Content-Type'] = 'multipart/form-data'
    end
    if @debug
      puts "status: #{res.status}"
      puts "headers: #{res.headers}"
      puts "body: #{res.body}"
    end
    raise APIError, "wrong status: #{res.status}" if res.status != 200
    JSON.parse(res.body)
  end
  def buy_drugs(headers, params)
    res = @client.post "/dopewars/api/game/#{params[:game_hash]}/buy" do |req|
      req.headers['Accept'] = 'application/json'
      req.headers['Authorization'] = "Bearer #{headers.fetch(:token)}"
      req.headers['Content-Type'] = 'multipart/form-data'
      data = {}
      data['drug_name'] = params.fetch(:name)
      data['quantity'] = params.fetch(:quantity)
      req.body = URI.encode_www_form(data)
    end
    if @debug
      puts "status: #{res.status}"
      puts "headers: #{res.headers}"
      puts "body: #{res.body}"
    end
    raise APIError, "wrong status: #{res.status}" if res.status != 200
    JSON.parse(res.body)
  end
  def sell_drugs(headers, params)
    res = @client.post "/dopewars/api/game/#{params[:game_hash]}/sell" do |req|
      req.headers['Accept'] = 'application/json'
      req.headers['Authorization'] = "Bearer #{headers.fetch(:token)}"
      req.headers['Content-Type'] = 'multipart/form-data'
      data = {}
      data['drug_name'] = params.fetch(:name)
      data['quantity'] = params.fetch(:quantity)
      req.body = URI.encode_www_form(data)
    end
    if @debug
      puts "status: #{res.status}"
      puts "headers: #{res.headers}"
      puts "body: #{res.body}"
    end
    raise APIError, "wrong status: #{res.status}" if res.status != 200
    JSON.parse(res.body)
  end
end
