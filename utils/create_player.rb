# Copyright (C) 2020 Thomas Baron
#
# This file is part of dopewars.
#
# dopewars is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3 of the License.
#
# dopewars is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with dopewars.  If not, see <https://www.gnu.org/licenses/>.
require 'faraday'
require 'json'
require 'uri'
require_relative 'lib/utils'
require_relative 'lib/api'
require_relative 'lib/context'
BASE_URL = "https://yellowcube.tom"
VERIFY_SSL = false
DEBUG = false
name = nil
password = nil
args = ARGV.dup
until args.empty?
  arg = args.shift
  if arg == '-n'
    name = String(args.shift)
  elsif arg == '-p'
    password = String(args.shift)
  end
end
unless name
  $stderr.puts 'missing name (-n)'
  exit 1
end
unless password
  $stderr.puts 'missing password (-p)'
  exit 1
end
ctx = Context.new({:base_url => BASE_URL, :verify_ssl => VERIFY_SSL, :debug => DEBUG})
ctx.create_player(name, password)
