function getCookie(name) {
  var v = document.cookie.match('(^|;) ?' + name + '=([^;]*)(;|$)');
  return v ? v[2] : null;
}
function setCookie(name, value, days) {
  var d = new Date();
  d.setTime(d.getTime() + (24 * 60 * 60 * 1000 * days));
  document.cookie = name + '=' + value + '; path=/; expires=' + d.toGMTString();
}
function deleteCookie(name) {
  setCookie(name, '', -1);
}
function formatCredits(credits) {
  var input = String((credits >= 0) ? credits : (-credits));
  var res = [];
  var c;
  var off;
  var idx = 0;
  while (idx < input.length) {
    c = (input.length - idx) % 3;
    if (c === 0) {
      if (res.length > 0) {
        res.push(',');
      }
      for (off = 0; off < 3; off += 1) {
        res.push(input[idx + off]);
      } // for
      idx += 3;
    } else {
      for (off = 0; off < c; off += 1) {
        res.push(input[idx + off]);
      } // for
      idx += c;
    }
  } // while
  return ((credits >= 0) ? '$' : '-$') + res.join('');
}
function formatFactor(factor) {
  var input = String(Math.round(factor * 100));
  if (input.length > 2) {
    return input.substr(0, input.length - 2) + '.' + input.substr(input.length - 2, 2);
  } else if (input.length > 1) {
    return '0.' + input;
  }
  return '0.' + input + '0';
}
