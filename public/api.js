var api = {};
/**
 * @param {object} opts
 * @param {object} opts.baseURL
 * @param {string} opts.playerName
 * @param {string} opts.playerPassword
 */
api.createPlayer = function (opts = {}) {
  return new Promise(function (resolve, reject) {
    var status = null;
    var headers = {
      Accept: 'application/json'
    };
    var formData = new FormData();
    formData.append('player_name', opts.playerName);
    formData.append('player_password', opts.playerPassword);
    fetch(opts.baseURL + '/api/player/create', {method: 'POST', headers: headers, body: formData})
      .then(function (res) {
        status = res.status;
        return res.json();
      })
      .then(function (res) {
        console.info('api - createPlayer', res);
        if (status !== 200) {
          return reject(res);
        }
        return resolve(res);
      })
      .catch(function (err) {
        console.error('api - createPlayer', err);
        return reject(err);
      });
  });
};
/**
 * @param {object} opts
 * @param {object} opts.baseURL
 * @param {string} opts.playerName
 * @param {string} opts.playerPassword
 */
api.authenticatePlayer = function (opts = {}) {
  return new Promise(function (resolve, reject) {
    var status = null;
    var headers = {
      Accept: 'application/json'
    };
    var formData = new FormData();
    formData.append('player_name', opts.playerName);
    formData.append('player_password', opts.playerPassword);
    fetch(opts.baseURL + '/api/player/authenticate', {method: 'POST', headers: headers, body: formData})
      .then(function (res) {
        status = res.status;
        return res.json();
      })
      .then(function (res) {
        console.info('api - authenticatePlayer', res);
        if (status !== 200) {
          return reject(res);
        }
        return resolve(res);
      })
      .catch(function (err) {
        console.error('api - authenticatePlayer', err);
        return reject(err);
      });
  });
};
/**
 * @param {object} opts
 * @param {object} opts.baseURL
 * @param {string} opts.token
 */
api.fetchProfile = function (opts = {}) {
  return new Promise(function (resolve, reject) {
    var status = null;
    var headers = {
      Accept: 'application/json',
      Authorization: 'Bearer ' + opts.token
    };
    fetch(opts.baseURL + '/api/player/profile', {method: 'GET', headers: headers})
      .then(function (res) {
        status = res.status;
        return res.json();
      })
      .then(function (res) {
        console.info('api - fetchProfile', res);
        if (status !== 200) {
          return reject(res);
        }
        return resolve(res);
      })
      .catch(function (err) {
        console.error('api - fetchProfile', err);
        return reject(err);
      });
  });
};
/**
 * @param {object} opts
 * @param {object} opts.baseURL
 * @param {string} opts.token
 * @param {string} opts.limit
 */
api.fetchGames = function (opts = {}) {
  return new Promise(function (resolve, reject) {
    var select = 'hightest_score';
    var limit = 10;
    var status = null;
    var headers = {
      Accept: 'application/json',
      Authorization: 'Bearer ' + opts.token
    };
    if (opts.select) {
      select = opts.select;
    }
    if (opts.limit) {
      limit = opts.limit;
    }
    fetch(opts.baseURL + '/api/game?select=' + select + '&limit=' + String(limit), {method: 'GET', headers: headers})
      .then(function (res) {
        status = res.status;
        return res.json();
      })
      .then(function (res) {
        console.info('api - fetchGames', res);
        if (status !== 200) {
          return reject(res);
        }
        return resolve(res);
      })
      .catch(function (err) {
        console.error('api - fetchGames', err);
        return reject(err);
      });
  });
};
/**
 * @param {object} opts
 * @param {object} opts.baseURL
 * @param {string} opts.token
 */
api.fetchConfig = function (opts = {}) {
  return new Promise(function (resolve, reject) {
    var status = null;
    var headers = {
      Accept: 'application/json',
      Authorization: 'Bearer ' + opts.token
    };
    fetch(opts.baseURL + '/api/game/config', {method: 'GET', headers: headers})
      .then(function (res) {
        status = res.status;
        return res.json();
      })
      .then(function (res) {
        console.info('api - fetchConfig', res);
        if (status !== 200) {
          return reject(res);
        }
        return resolve(res);
      })
      .catch(function (err) {
        console.error('api - fetchConfig', err);
        return reject(err);
      });
  });
};
/**
 * @param {object} opts
 * @param {object} opts.baseURL
 * @param {string} opts.token
 * @param {boolean} opts.custom - Whether to create a non-standard game.
 * @param {number} opts.cash - The initial amount of cash.
 * @param {number} opts.debt - The initial debt
 * @param {number} opts.sidekicks - The initial number of sidekicks.
 * @param {number} opts.sidekickComp - The compensation of each sidekick.
 * @param {number} opts.sidekickProfitSharing - The profit sharing of each sidekick.
 * @param {number} opts.bankInterest - The bank interest.
 * @param {number} opts.debtInterest - The debt interest.
 * @param {number} opts.hireSidekick - Whether to be possible to hire a new sidekick everywhere.
 * @param {boolean} opts.offerEveryDrug - Whether to offer every drug.
 * @param {boolean} opts.offerCheapDrug - Whether to offer at least one cheap drug.
 * @param {boolean} opts.offerExpensiveDrug - whether to offer at least one expensive drug.
 */
api.createGame = function (opts = {}) {
  return new Promise(function (resolve, reject) {
    var status = null;
    var headers = {
      Accept: 'application/json',
      Authorization: 'Bearer ' + opts.token
    };
    var formData = null;
    if (opts.custom) {
      formData = new FormData();
      formData.append('custom', '1');
      if (opts.turnLimit) {
        formData.append('turn_limit', String(opts.turnLimit));
      }
      if (opts.cash) {
        formData.append('cash', String(opts.cash));
      }
      if (opts.debt) {
        formData.append('debt', String(opts.debt));
      }
      if (opts.sidekicks) {
        formData.append('sidekicks', String(opts.sidekicks));
      }
      if (opts.sidekickComp) {
        formData.append('sidekick_comp', String(opts.sidekickComp));
      }
      if (opts.sidekickProfitSharing) {
        formData.append('sidekick_profit_sharing', String(opts.sidekickProfitSharing));
      }
      if (opts.bankInterest) {
        formData.append('bank_interest', String(opts.bankInterest));
      }
      if (opts.debtInterest) {
        formData.append('debt_interest', String(opts.debtInterest));
      }
      if (opts.hireSidekick) {
        formData.append('hire_sidekick', '1');
      }
      if (opts.offerEveryDrug) {
        formData.append('offer_every_drug', '1');
      }
      if (opts.offerCheapDrug) {
        formData.append('offer_cheap_drug', '1');
      }
      if (opts.offerExpensiveDrug) {
        formData.append('offer_expensive_drug', '1');
      }
    }
    fetch(opts.baseURL + '/api/game/create', {method: 'POST', headers: headers, body: formData})
      .then(function (res) {
        status = res.status;
        return res.json();
      })
      .then(function (res) {
        console.info('api - createGame', res);
        if (status !== 200) {
          return reject(res);
        }
        return resolve(res);
      })
      .catch(function (err) {
        console.error('api - createGame', err);
        return reject(err);
      });
  });
};
/**
 * @param {object} opts
 * @param {object} opts.baseURL
 * @param {string} opts.token
 * @param {string} opts.gameHash
 */
api.fetchInventory = function (opts = {}) {
  return new Promise(function (resolve, reject) {
    var status = null;
    var headers = {
      Accept: 'application/json',
      Authorization: 'Bearer ' + opts.token
    };
    fetch(opts.baseURL + '/api/game/' + opts.gameHash + '/inventory', {method: 'GET', headers: headers})
      .then(function (res) {
        status = res.status;
        return res.json();
      })
      .then(function (res) {
        console.info('api - fetchInventory', res);
        if (status !== 200) {
          return reject(res);
        }
        return resolve(res);
      })
      .catch(function (err) {
        console.error('api - fetchInventory', err);
        return reject(err);
      });
  });
};
/**
 * @param {object} opts
 * @param {object} opts.baseURL
 * @param {string} opts.token
 * @param {string} opts.gameHash
 * @param {string} opts.locationName
 */
api.changeLocation = function (opts = {}) {
  return new Promise(function (resolve, reject) {
    var status = null;
    var headers = {
      Accept: 'application/json',
      Authorization: 'Bearer ' + opts.token
    };
    var formData = new FormData();
    formData.append('location_name', opts.locationName);
    fetch(opts.baseURL + '/api/game/' + opts.gameHash + '/jet', {method: 'POST', headers: headers, body: formData})
      .then(function (res) {
        status = res.status;
        return res.json();
      })
      .then(function (res) {
        console.info('api - changeLocation', res);
        if (status !== 200) {
          return reject(res);
        }
        return resolve(res);
      })
      .catch(function (err) {
        console.error('api - changeLocation', err);
        return reject(err);
      });
  });
};
/**
 * @param {object} opts
 * @param {object} opts.baseURL
 * @param {string} opts.token
 * @param {string} opts.gameHash
 * @param {string} opts.cash
 */
api.makeDeposit = function (opts = {}) {
  return new Promise(function (resolve, reject) {
    var status = null;
    var headers = {
      Accept: 'application/json',
      Authorization: 'Bearer ' + opts.token
    };
    var formData = new FormData();
    formData.append('cash', opts.cash);
    fetch(opts.baseURL + '/api/game/' + opts.gameHash + '/put', {method: 'POST', headers: headers, body: formData})
      .then(function (res) {
        status = res.status;
        return res.json();
      })
      .then(function (res) {
        console.info('api - makeDeposit', res);
        if (status !== 200) {
          return reject(res);
        }
        return resolve(res);
      })
      .catch(function (err) {
        console.error('api - makeDeposit', err);
        return reject(err);
      });
  });
};
/**
 * @param {object} opts
 * @param {object} opts.baseURL
 * @param {string} opts.token
 * @param {string} opts.gameHash
 * @param {string} opts.cash
 */
api.makeWithdraw = function (opts = {}) {
  return new Promise(function (resolve, reject) {
    var status = null;
    var headers = {
      Accept: 'application/json',
      Authorization: 'Bearer ' + opts.token
    };
    var formData = new FormData();
    formData.append('cash', opts.cash);
    fetch(opts.baseURL + '/api/game/' + opts.gameHash + '/take', {method: 'POST', headers: headers, body: formData})
      .then(function (res) {
        status = res.status;
        return res.json();
      })
      .then(function (res) {
        console.info('api - makeDeposit', res);
        if (status !== 200) {
          return reject(res);
        }
        return resolve(res);
      })
      .catch(function (err) {
        console.error('api - makeDeposit', err);
        return reject(err);
      });
  });
};
/**
 * @param {object} opts
 * @param {object} opts.baseURL
 * @param {string} opts.token
 * @param {string} opts.gameHash
 * @param {string} opts.cash
 */
api.payDebt = function (opts = {}) {
  return new Promise(function (resolve, reject) {
    var status = null;
    var headers = {
      Accept: 'application/json',
      Authorization: 'Bearer ' + opts.token
    };
    var formData = new FormData();
    formData.append('cash', opts.cash);
    fetch(opts.baseURL + '/api/game/' + opts.gameHash + '/pay', {method: 'POST', headers: headers, body: formData})
      .then(function (res) {
        status = res.status;
        return res.json();
      })
      .then(function (res) {
        console.info('api - payDebt', res);
        if (status !== 200) {
          return reject(res);
        }
        return resolve(res);
      })
      .catch(function (err) {
        console.error('api - payDebt', err);
        return reject(err);
      });
  });
};
/**
 * @param {object} opts
 * @param {object} opts.baseURL
 * @param {string} opts.token
 * @param {string} opts.gameHash
 */
api.hireSidekick = function (opts = {}) {
  return new Promise(function (resolve, reject) {
    var status = null;
    var headers = {
      Accept: 'application/json',
      Authorization: 'Bearer ' + opts.token
    };
    fetch(opts.baseURL + '/api/game/' + opts.gameHash + '/hire', {method: 'POST', headers: headers})
      .then(function (res) {
        status = res.status;
        return res.json();
      })
      .then(function (res) {
        console.info('api - hireSidekick', res);
        if (status !== 200) {
          return reject(res);
        }
        return resolve(res);
      })
      .catch(function (err) {
        console.error('api - hireSidekick', err);
        return reject(err);
      });
  });
};
/**
 * @param {object} opts
 * @param {object} opts.baseURL
 * @param {string} opts.token
 * @param {string} opts.gameHash
 * @param {string} opts.drugName
 * @param {number} opts.quantity
 */
api.buyDrug = function (opts = {}) {
  return new Promise(function (resolve, reject) {
    var status = null;
    var headers = {
      Accept: 'application/json',
      Authorization: 'Bearer ' + opts.token
    };
    var formData = new FormData();
    formData.append('drug_name', opts.drugName);
    formData.append('quantity', opts.quantity);
    fetch(opts.baseURL + '/api/game/' + opts.gameHash + '/buy', {method: 'POST', headers: headers, body: formData})
      .then(function (res) {
        status = res.status;
        return res.json();
      })
      .then(function (res) {
        console.info('api - buyDrug', res);
        if (status !== 200) {
          return reject(res);
        }
        return resolve(res);
      })
      .catch(function (err) {
        console.error('api - buyDrug', err);
        return reject(err);
      });
  });
};
/**
 * @param {object} opts
 * @param {object} opts.baseURL
 * @param {string} opts.token
 * @param {string} opts.gameHash
 * @param {string} opts.drugName
 * @param {number} opts.quantity
 */
api.sellDrug = function (opts = {}) {
  return new Promise(function (resolve, reject) {
    var status = null;
    var headers = {
      Accept: 'application/json',
      Authorization: 'Bearer ' + opts.token
    };
    var formData = new FormData();
    formData.append('drug_name', opts.drugName);
    formData.append('quantity', opts.quantity);
    fetch(opts.baseURL + '/api/game/' + opts.gameHash + '/sell', {method: 'POST', headers: headers, body: formData})
      .then(function (res) {
        status = res.status;
        return res.json();
      })
      .then(function (res) {
        console.info('api - sellDrug', res);
        if (status !== 200) {
          return reject(res);
        }
        return resolve(res);
      })
      .catch(function (err) {
        console.error('api - sellDrug', err);
        return reject(err);
      });
  });
};
/**
 * @param {object} opts
 * @param {object} opts.baseURL
 * @param {string} opts.token
 * @param {string} opts.gameHash
 * @param {string} opts.drugName
 * @param {number} opts.quantity
 */
api.dropDrug = function (opts = {}) {
  return new Promise(function (resolve, reject) {
    var status = null;
    var headers = {
      Accept: 'application/json',
      Authorization: 'Bearer ' + opts.token
    };
    var formData = new FormData();
    formData.append('drug_name', opts.drugName);
    formData.append('quantity', opts.quantity);
    fetch(opts.baseURL + '/api/game/' + opts.gameHash + '/drop', {method: 'POST', headers: headers, body: formData})
      .then(function (res) {
        status = res.status;
        return res.json();
      })
      .then(function (res) {
        console.info('api - dropDrug', res);
        if (status !== 200) {
          return reject(res);
        }
        return resolve(res);
      })
      .catch(function (err) {
        console.error('api - dropDrug', err);
        return reject(err);
      });
  });
};
