# Copyright (C) 2020 Thomas Baron
#
# This file is part of dopewars.
#
# dopewars is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3 of the License.
#
# dopewars is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with dopewars.  If not, see <https://www.gnu.org/licenses/>.
module GameHelper
  TURN_LIMIT = 31
  EVENT_KIND_OTHER = 1
  EVENT_KIND_STORY = 2
  EVENT_KIND_ACTION = 3
  DEFAULT_CASH = 2_000
  DEFAULT_DEBT = 5_500
  DEFAULT_SIDEKICKS = 9
  DEFAULT_SIDEKICK_COMP = 50_000
  DEFAULT_SIDEKICK_PROFIT_SHARING = 2
  DEFAULT_BANK_INTEREST = 5
  DEFAULT_DEBT_INTEREST = 10
  def eval_game_creation_count(player_id, since)
    threshold = Time.now.tv_sec - since
    Game.where(['created_at >= ? AND player_id = ?', threshold, player_id]).count
  end
  # Creates a new game.
  # @param player_id [Integer]
  # @param opts [Hash]
  # @option opts [Boolean] :custom
  # @option opts [Integer] :cash
  # @option opts [Integer] :debt
  # @option opts [Integer] :sidekicks
  # @option opts [Integer] :sidekick_comp
  # @option opts [Integer] :sidekick_profit_sharing
  # @option opts [Integer] :bank_interest
  # @option opts [Integer] :debt_interest
  # @option opts [Boolean] :hire_sidekick
  # @option opts [Boolean] :offer_every_drug
  # @option opts [Boolean] :offer_cheap_drug
  # @option opts [Boolean] :offer_expensive_drug
  # @return [Game]
  def create_game(player_id, opts = {})
    standard = !opts.fetch(:custom, false)
    new_game = nil
    new_play = nil
    config = nil
    ActiveRecord::Base.transaction do
      if standard
        # standard game
        crew = DEFAULT_SIDEKICK_COMP * DEFAULT_SIDEKICKS
        new_game = Game.create({:public_id => generate_hash, :active => 1, :standard => 1, :cash => DEFAULT_CASH, :bank => 0, :debt => DEFAULT_DEBT, :crew => crew, :player_id => player_id})
        Expiration.create({:expired_at => Time.now.to_i + 3_600, :object_name => 'game', :object_id => new_game.id})
        new_play = Play.create({:turn => 1, :location_kind => location_specs.first.kind, :health => 100, :sidekicks => DEFAULT_SIDEKICKS, :game_id => new_game.id})
      else
        # custom game
        cash = opts.fetch(:cash, DEFAULT_CASH)
        debt = opts.fetch(:debt, DEFAULT_DEBT)
        sidekicks = opts.fetch(:sidekicks, DEFAULT_SIDEKICKS)
        sidekick_comp = opts.fetch(:sidekick_comp, DEFAULT_SIDEKICK_COMP)
        crew = sidekick_comp * sidekicks
        config = {}
        config['turn_limit'] = opts.fetch(:turn_limit, TURN_LIMIT)
        config['cash'] = cash
        config['debt'] = debt
        config['sidekicks'] = sidekicks
        config['sidekick_comp'] = sidekick_comp
        config['sidekick_profit_sharing'] = opts.fetch(:sidekick_profit_sharing, DEFAULT_SIDEKICK_PROFIT_SHARING)
        config['bank_interest'] = opts.fetch(:bank_interest, DEFAULT_BANK_INTEREST)
        config['debt_interest'] = opts.fetch(:debt_interest, DEFAULT_DEBT_INTEREST)
        config['hire_sidekick'] = opts.fetch(:hire_sidekick, false)
        config['offer_every_drug'] = opts.fetch(:offer_every_drug, false)
        config['offer_cheap_drug'] = opts.fetch(:offer_cheap_drug, false)
        config['offer_expensive_drug'] = opts.fetch(:offer_expensive_drug, false)
        new_game = Game.create({:public_id => generate_hash, :active => 1, :standard => 0, :cash => cash, :bank => 0, :debt => debt, :crew => crew, :config => config.to_json, :player_id => player_id})
        Expiration.create({:expired_at => Time.now.to_i + 3_600, :object_name => 'game', :object_id => new_game.id})
        new_play = Play.create({:turn => 1, :location_kind => location_specs.first.kind, :health => 100, :sidekicks => sidekicks, :game_id => new_game.id})
      end
      location_specs.each do |loc|
        Location.create({:kind => loc.kind, :threat => 0, :game_id => new_game.id})
      end
      drug_specs.each do |drug|
        Business.create({:drug_kind => drug.kind, :buy_quantity => 0, :buy_value => 0, :sell_quantity => 0, :sell_value => 0, :game_id => new_game.id})
        InventoryItem.create({:drug_kind => drug.kind, :quantity => 0, :value => 0, :game_id => new_game.id})
      end
      create_event new_game, new_play, "You started with a nice crew, and an even nicer debt!", :story
      create_offers(new_game, new_play, config, location_specs.first)
    end # transaction
    new_game
  end
  # Creates a new turn for a given game.
  # @param game [Game]
  # @param play [Play]
  # @paran config [Hash] Custom game config, nil for standard game.
  # @param location [LocationSpec]
  # @return [Game]
  def create_turn(game, play, config, location_spec)
    raise StandardError, 'cannot create turn' if play.turn == nil
    ActiveRecord::Base.transaction do
      Offer.where("game_id = #{game.id}").destroy_all
      Location.where("game_id = #{game.id}").each do |loc|
        next if (loc.threat == 0) || (loc.kind == play.location_kind) || (loc.kind == location_spec.kind)
        loc.threat -= (loc.threat.to_f / 10.0).ceil
        loc.save
      end
      play.turn += 1
      play.location_kind = location_spec.kind
      if game.bank > 0
        factor = 1.05
        factor = 1.0 + (config['bank_interest'].to_f / 100.0) if config
        game.bank = (game.bank.to_f * factor).floor
      end
      if game.debt > 0
        factor = 1.1
        factor = 1.0 + (config['debt_interest'].to_f / 100.0) if config
        game.debt = (game.debt.to_f * factor).floor
      end
      location = Location.where("kind = #{play.location_kind} AND game_id = #{game.id}").take
      if location.threat > 0
        damage = random_number((location.threat >= 2) ? (location.threat / 2) : 1, location.threat)
        create_event game, play, "You took a hit (#{damage} health loss)", :story
        play.health -= damage
        dead_sidekicks = 0
        dropped_drugs = {}
        while (play.sidekicks > 0) && (play.health <= 0)
          dead_sidekicks += 1
          play.sidekicks -= 1
          play.health += 100
          space = 10 + (play.sidekicks * 10)
          loop do
            occ = InventoryItem.where("game_id = #{game.id}").sum("quantity")
            break if occ <= space
            item = InventoryItem.where("quantity > 0 AND game_id = #{game.id}").order("value ASC").take
            unit_value = (item.value.to_f / item.quantity.to_f).floor
            if dropped_drugs.has_key? item.drug_kind
              dropped_drugs[item.drug_kind][:quantity] += 1
              dropped_drugs[item.drug_kind][:value] += unit_value
            else
              dropped_drugs[item.drug_kind] = {:kind => item.drug_kind, :quantity => 1, :value => unit_value}
            end
            item.quantity -= 1
            if item.quantity == 0
              item.value = 0
            else
              item.value -= unit_value
            end
            item.save
          end
        end
        if dead_sidekicks > 1
          create_event game, play, "You lost #{dead_sidekicks} sidekicks (#{dead_sidekicks * 10} space loss)", :action
        elsif dead_sidekicks == 1
          create_event game, play, "You lost a sidekick (10 space loss)", :action
        end
        dropped_drugs.values.each do |e|
          drug_spec = drug_specs.find { |d| d.kind == e[:kind] }
          unit_s = "1 unit"
          unit_s = "#{e[:quantity]} units" if e[:quantity] > 1
          create_event game, play, "You dropped #{unit_s} of #{drug_spec.name} ($#{format_credit(e[:value])} loss)", :action
        end
      end
      if (config && config['hire_sidekick']) || location_spec.hire || (random_number(0, 2) == 0)
        sidekick_comp = DEFAULT_SIDEKICK_COMP
        sidekick_comp = config['sidekick_comp'] if config
        play.sidekick_comp = random_number(sidekick_comp, sidekick_comp + (1_000 * play.turn))
        create_event game, play, "Hey, I can join your crew for $#{format_credit(play.sidekick_comp)}", :story
      else
        play.sidekick_comp = nil
      end
      play.save
      game.save
      if play.health <= 0
        # game over
        create_event game, play, "You are dead", :story
        terminate_game(game)
      else
        # game continues
        create_offers(game, play, config, location_spec)
      end
    end # transaction
    game
  end
  # Creates offers.
  # @param game [Game]
  # @param play [Play]
  # @param config [Hash]
  # @param location_spec [LocationSpec]
  def create_offers(game, play, config, location_spec)
    OfferGenerator.offers(drug_specs, config, location_spec).each do |offer|
      if offer[:cheap]
        create_event game, play, "#{offer[:drug_spec].name} is really cheap here", :story
        total_space = 10 + (play.sidekicks * 10)
        occupied_space = InventoryItem.where("game_id = #{game.id}").sum("quantity")
        if (total_space - occupied_space) >= 5
          # free drug units
          quantity = random_number(2, 5)
          value = (offer[:drug_spec].min / 2) * quantity
          item = InventoryItem.where("drug_kind = #{offer[:drug_spec].kind} AND game_id = #{game.id}").take
          item.quantity += quantity
          item.save
          create_event game, play, "You found #{quantity} units of #{offer[:drug_spec].name} worth $#{format_credit(value)}", :action
        end
      elsif offer[:expensive]
        create_event game, play, "#{offer[:drug_spec].name} is very expensive here", :story
      end
      Offer.create({:drug_kind => offer[:drug_spec].kind, :unit_price => offer[:unit_price], :game_id => game.id})
    end
  end
  class OfferGenerator
    def initialize(drug_specs, config, location_spec)
      @drug_specs = drug_specs.shuffle
      if config
        @offer_every_drug = config['offer_every_drug']
        @offer_cheap_drug = config['offer_cheap_drug']
        @offer_expensive_drug = config['offer_expensive_drug']
      else
        @offer_every_drug = false
        @offer_cheap_drug = false
        @offer_expensive_drug = false
      end
      @location_spec = location_spec
      @randomizer = ApplicationHelper::Randomizer.new
    end
    # Returns a random integer.
    # @param min [Integer] Mininum value, inclusive.
    # @param max [Integer] Maximum value, inclusive.
    # @return [Integer]
    def random_number(min, max)
      @randomizer.random_number(max - min + 1) + min
    end
    # Returns a drug spec.
    # @param opts [Hash]
    # @option opts [Boolean] :cheap_only
    # @option opts [Boolean] :expensive_only
    # @option opts [Boolean] :cheap_and_expensive_only
    # @return [Array]
    def select_drug_spec(opts = {})
      if opts.fetch(:cheap_and_expensive_only, false)
        @drug_specs.select { |s| s.cheap || s.expensive } .first
      elsif opts.fetch(:cheap_only, false)
        @drug_specs.select { |s| s.cheap } .first
      elsif opts.fetch(:expensive_only, false)
        @drug_specs.select { |s| s.expensive } .first
      else
        @drug_specs.first
      end
    end
    def create_cheap_offer
      drug_spec = select_drug_spec :cheap_only => true
      @drug_specs.delete_if { |s| s.kind == drug_spec.kind }
      price = random_number(drug_spec.min, drug_spec.max)
      price /= 2 while price >= drug_spec.min
      @offers << {:drug_spec => drug_spec, :cheap => true, :unit_price => price}
    end
    def create_expensive_offer
      drug_spec = select_drug_spec :expensive_only => true
      @drug_specs.delete_if { |s| s.kind == drug_spec.kind }
      price = random_number(drug_spec.min, drug_spec.max)
      price *= 2 while price <= drug_spec.max
      @offers << {:drug_spec => drug_spec, :expensive => true, :unit_price => price}
    end
    def create_special_offer
      drug_spec = select_drug_spec :cheap_and_expensive_only => true
      @drug_specs.delete_if { |s| s.kind == drug_spec.kind }
      price = random_number(drug_spec.min, drug_spec.max)
      new_offer = {:drug_spec => drug_spec}
      if drug_spec.cheap
        price = price / 2
        new_offer[:cheap] = true if price < drug_spec.min
      elsif drug_spec.expensive
        price = price * 2
        new_offer[:expensive] = true if price > drug_spec.max
      end
      new_offer[:unit_price] = price
      @offers << new_offer
    end
    def create_offer
      drug_spec = select_drug_spec
      @drug_specs.delete_if { |s| s.kind == drug_spec.kind }
      price = random_number(drug_spec.min, drug_spec.max)
      @offers << {:drug_spec => drug_spec, :unit_price => price}
    end
    def run
      @drugs = {}
      if @offer_every_drug
        @offer_count = @drug_specs.length
      else
        @offer_count = random_number(@location_spec.min_drugs, @location_spec.max_drugs)
      end
      @offers = []
      create_cheap_offer if @offer_cheap_drug
      create_expensive_offer if @offer_expensive_drug
      create_special_offer
      create_offer while @offers.length < @offer_count
      @offers
    end
    def self.offers(drug_specs, config, location_spec)
      OfferGenerator.new(drug_specs, config, location_spec).run
    end
  end
  # Creates a new event.
  # @param game [Game]
  # @param play [Play]
  # @param description [String]
  # @param kind [Symbol] `:story`, `:action`.
  def create_event(game, play, description, kind)
    evt_kind = EVENT_KIND_OTHER
    if kind == :story
      evt_kind = EVENT_KIND_STORY
    elsif kind == :action
      evt_kind = EVENT_KIND_ACTION
    end
    Event.create({:description => description, :kind => evt_kind, :location_kind => play.location_kind, :turn => play.turn, :game_id => game.id})
  end
  # Terminates a given game.
  # @param game [Game]
  # @param play [Play]
  # @return [Game]
  def terminate_game(game, play)
    stock = 0
    InventoryItem.where("quantity > 0 AND game_id = #{game.id}").each do |item|
      drug_spec = drug_specs.find { |d| d.kind == item.drug_kind }
      unit_value = drug_spec.min
      unit_value /= 2 if drug_spec.cheap
      value = unit_value * item.quantity
      stock += value
      business = Business.where("drug_kind = #{drug_spec.kind} AND game_id = #{game.id}").take
      business.sell_quantity += item.quantity
      business.sell_value += value
      business.save
      unit_s = "unit"
      is = "is"
      if item.quantity > 1
        unit_s = "#{item.quantity} units"
        is = "are"
      end
      create_event game, play, "Your #{unit_s} of #{drug_spec.name} #{is} worth $#{format_credit(value)}", :action
    end
    Event.where("kind = #{EVENT_KIND_STORY} AND game_id = #{game.id}").destroy_all
    InventoryItem.where("game_id = #{game.id}").destroy_all
    Location.where("game_id = #{game.id}").destroy_all
    Offer.where("game_id = #{game.id}").destroy_all
    Play.where("game_id = #{game.id}").destroy_all
    game.active = 0
    game.stock = stock
    game.crew += (stock.to_f * play.sidekicks.to_f * 0.02).ceil if stock > 0
    game.score = game.cash + game.bank - game.debt + game.stock - game.crew
    game.save
    game
  end
  # Returns the inventory of a given game.
  # @param game [Game]
  # @return [Hash]
  def inventory(game)
    res = {}
    res[:hash] = game.public_id
    res[:player_name] = game.player.name
    res[:cash] = game.cash
    res[:bank] = game.bank
    res[:debt] = game.debt
    res[:crew] = game.crew
    if game.active != 0
      # game ongoing
      play = game.play
      location_spec = location_specs.find { |l| l.kind == play.location_kind }
      config = nil
      if (game.standard == 0) && game.config
        config = JSON.parse(game.config)
      end
      res[:active] = true
      res[:turn] = play.turn
      res[:turn_limit] = TURN_LIMIT
      res[:turn_limit] = config['turn_limit'] if config
      res[:locations] = []
      Location.where("game_id = #{game.id}").order("kind ASC").each do |loc|
        loc_spec = location_specs.find { |l| l.kind == loc.kind }
        res[:locations] << {:name => loc_spec.name, :threat => loc.threat}
      end
      res[:current_location] = {:name => location_spec.name, :bank => location_spec.bank, :debt => location_spec.debt}
      res[:space] = 10 + (play.sidekicks * 10)
      res[:sidekicks] = play.sidekicks
      res[:health] = play.health
      res[:sidekick_comp] = play.sidekick_comp if play.sidekick_comp
      res[:offers] = []
      Offer.where("game_id = #{game.id}").order("drug_kind ASC").each do |o|
        drug_spec = drug_specs.find { |s| s.kind == o.drug_kind }
        rating = nil
        if o.unit_price < drug_spec.min
          rating = '-'
        elsif o.unit_price > drug_spec.max
          rating = '+'
        else
          span = (drug_spec.max - drug_spec.min).to_f / 11.0
          rating = ((o.unit_price - drug_spec.min).to_f / span).floor
          rating = 10 if rating > 10
        end
        res[:offers] << {:drug_name => drug_spec.name, :unit_price => o.unit_price, :rating => rating}
      end
      res[:items] = []
      InventoryItem.where("game_id = #{game.id}").order("drug_kind ASC").each do |item|
        next if item.quantity == 0
        drug_spec = drug_specs.find { |s| s.kind == item.drug_kind }
        res[:space] -= item.quantity
        res[:items] << {:drug_name => drug_spec.name, :quantity => item.quantity, :value => item.value}
      end
      res[:events] = []
      Event.where("game_id = #{game.id}").order("id DESC").each do |event|
        loc_spec = location_specs.find { |l| l.kind == event.location_kind }
        res[:events] << {:turn => event.turn, :location_name => loc_spec.name, :description => event.description}
      end
      res[:events].reverse!
    else
      # game is not active
      res[:active] = false
      res[:standard] = game.standard != 0
      res[:when] = Time.at(game.updated_at).strftime("%Y-%m-%d")
      res[:score] = game.score
      res[:stock] = game.stock
      res[:statistics] = []
      Business.where("game_id = #{game.id}").order("drug_kind ASC").each do |b|
        drug_spec = drug_specs.find { |s| s.kind == b.drug_kind }
        res[:statistics] << {:drug_name => drug_spec.name, :buy_quantity => b.buy_quantity, :buy_value => b.buy_value, :sell_quantity => b.sell_quantity, :sell_value => b.sell_value}
      end
      res[:events] = []
      Event.where("game_id = #{game.id}").order("id DESC").each do |event|
        loc_spec = location_specs.find { |l| l.kind == event.location_kind }
        res[:events] << {:turn => event.turn, :location_name => loc_spec.name, :description => event.description}
      end
      res[:events].reverse!
    end
    res
  end
  # Makes a deposit.
  # @param game [Game]
  # @param cash [Integer]
  def make_deposit(game, cash)
    raise StandardError, 'not enough money' if game.cash < cash
    play = game.play
    ActiveRecord::Base.transaction do
      create_event game, play, "You made a $#{format_credit(cash)} deposit", :action
      game.cash -= cash
      game.bank += cash
      game.save
    end # transaction
  end
  # Makes a withdraw.
  # @param game [Game]
  # @param cash [Integer]
  def make_withdraw(game, cash)
    raise StandardError, 'not enough money' if game.bank < cash
    play = game.play
    ActiveRecord::Base.transaction do
      create_event game, play, "You made a $#{format_credit(cash)} withdraw", :action
      game.cash += cash
      game.bank -= cash
      game.save
    end # transaction
  end
  # Pays back the debt.
  # @param game [Game]
  # @param cash [Integer]
  def pay_debt(game, cash)
    raise StandardError, 'not enough money' if game.cash < cash
    raise StandardError, 'too much money' if cash > game.debt
    play = game.play
    ActiveRecord::Base.transaction do
      game.cash -= cash
      game.debt -= cash
      game.save
      rem = (game.debt > 0) ? "$#{format_credit(game.debt)} remaining" : "no debt anymore"
      create_event game, play, "You paid back your loaner $#{format_credit(cash)} (#{rem})", :action
    end # transaction
  end
  # Evaluates the threat for a given (drug) value.
  #      value  incr  total
  # ----------  ----  -----
  #        500     5      5
  #      5_000     5     10
  #     50_000     5     15
  #    500_000     5     20
  #  5_000_000     5     25
  # 50_000_000     5     30
  # @param value [Integer]
  # @return [Integer]
  def eval_threat(value)
    value = value.to_f
    res = 0
    loop do
      break if value < 500.0
      value /= 10.0
      res += 5
    end
    res
  end
  # Increases the threat on the current location.
  # @param game [Game]
  # @param play [Play]
  def incr_threat(game, play, threat)
    location = Location.where("kind = #{play.location_kind} AND game_id = #{game.id}").take
    location.threat += threat
    location.save
  end
  # Hires a sidekick.
  # @param game [Game]
  def hire_sidekick(game)
    play = game.play
    raise StandardError, 'no sidekick to hire' if play.sidekick_comp == nil
    raise StandardError, 'not enough money' if game.cash < play.sidekick_comp
    ActiveRecord::Base.transaction do
      create_event game, play, "You hired a sidekick for $#{format_credit(play.sidekick_comp)} (10 space gain)", :action
      game.cash -= play.sidekick_comp
      play.sidekicks += 1
      play.sidekick_comp = nil
      play.save
      game.save
    end # transaction
  end
  # Returns the available space.
  # @param game [Game]
  # @param play [Play]
  # @return [Integer]
  def eval_available_space(game, play)
    total_space = 10 + (play.sidekicks * 10)
    occupied_space = InventoryItem.where("game_id = #{game.id}").sum("quantity")
    total_space - occupied_space
  end
  # Buys a given amount of drugs.
  # @param game [Game]
  # @param drug_name [String]
  # @param quantity [Integer]
  def buy_drug(game, drug_name, quantity)
    drug_spec = drug_specs.find { |s| s.name == drug_name }
    raise StandardError, 'no such drug' unless drug_spec
    offer = Offer.where("drug_kind = #{drug_spec.kind} AND game_id = #{game.id}").take
    raise StandardError, 'no such offer' unless offer
    raise StandardError, 'not enough quantity' unless quantity > 0
    value = offer.unit_price * quantity
    raise StandardError, 'not enough cash' if value > game.cash
    item = InventoryItem.where("drug_kind = #{drug_spec.kind} AND game_id = #{game.id}").take
    raise StandardError, 'no such item' unless item
    play = game.play
    available_space = eval_available_space game, play
    raise StandardError, 'not enough space' unless quantity <= available_space
    play = game.play
    ActiveRecord::Base.transaction do
      incr_threat game, play, eval_threat(value)
      game.cash -= value
      game.save
      item.quantity += quantity
      item.value += value
      item.save
      business = Business.where("drug_kind = #{drug_spec.kind} AND game_id = #{game.id}").take
      business.buy_quantity += quantity
      business.buy_value += value
      business.save
      unit_s = "1 unit"
      unit_s = "#{quantity} units" if quantity > 1
      create_event game, play, "You bought #{unit_s} of #{drug_name} for $#{format_credit(value)}", :action
    end # transaction
  end
  # Sells a given amount of drugs.
  # @param game [Game]
  # @param drug_name [String]
  # @param quantity [Integer]
  def sell_drug(game, drug_name, quantity)
    drug_spec = drug_specs.find { |s| s.name == drug_name }
    raise StandardError, 'no such drug' unless drug_spec
    offer = Offer.where("drug_kind = #{drug_spec.kind} AND game_id = #{game.id}").take
    raise StandardError, 'no such offer' unless offer
    value = offer.unit_price * quantity
    item = InventoryItem.where("drug_kind = #{drug_spec.kind} AND game_id = #{game.id}").take
    raise StandardError, 'no such item' unless item
    raise StandardError, 'not enough quantity' unless quantity > 0
    raise StandardError, 'no such quantity' unless quantity <= item.quantity
    config = nil
    if (game.standard == 0) && game.config
      config = JSON.parse(game.config)
    end
    play = game.play
    ActiveRecord::Base.transaction do
      incr_threat game, play, eval_threat(value)
      unit_value = (item.value.to_f / item.quantity.to_f).floor
      game.cash += value
      profit = value - (unit_value * quantity)
      factor = 0.02
      factor = (config['sidekick_profit_sharing'].to_f / 100.0) if config
      game.crew += (profit.to_f * play.sidekicks.to_f * factor).ceil if profit > 0
      game.save
      item.quantity -= quantity
      if item.quantity == 0
        item.value = 0
      else
        item.value -= (unit_value * quantity)
      end
      item.save
      business = Business.where("drug_kind = #{drug_spec.kind} AND game_id = #{game.id}").take
      business.sell_quantity += quantity
      business.sell_value += value
      business.save
      unit_s = "1 unit"
      unit_s = "#{quantity} units" if quantity > 1
      if profit > 0
        create_event game, play, "You sold #{unit_s} of #{drug_name} for $#{format_credit(value)} ($#{format_credit(profit)} profit)", :action
      elsif profit < 0
        create_event game, play, "You sold #{unit_s} of #{drug_name} for $#{format_credit(value)} ($#{format_credit(-profit)} loss)", :action
      else
        create_event game, play, "You sold #{unit_s} of #{drug_name} for $#{format_credit(value)} (no profit)", :action
      end
    end # transaction
  end
  # Drops a given amount of drugs.
  # @param game [Game]
  # @param drug_name [String]
  # @param quantity [Integer]
  def drop_drug(game, drug_name, quantity)
    drug_spec = drug_specs.find { |s| s.name == drug_name }
    raise StandardError, 'no such drug' unless drug_spec
    item = InventoryItem.where("drug_kind = #{drug_spec.kind} AND game_id = #{game.id}").take
    raise StandardError, 'no such item' unless item
    raise StandardError, 'not enough quantity' unless quantity > 0
    raise StandardError, 'no such quantity' unless quantity <= item.quantity
    play = game.play
    ActiveRecord::Base.transaction do
      unit_value = (item.value.to_f / item.quantity.to_f).floor
      item.quantity -= quantity
      if item.quantity == 0
        item.value = 0
      else
        item.value -= (unit_value * quantity)
      end
      item.save
      unit_s = "1 unit"
      unit_s = "#{quantity} units" if quantity > 1
      create_event game, play, "You dropped #{unit_s} of #{drug_name} ($#{format_credit(unit_value * quantity)} loss)", :action
    end # transaction
  end
end
