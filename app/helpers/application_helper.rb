# Copyright (C) 2020 Thomas Baron
#
# This file is part of dopewars.
#
# dopewars is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3 of the License.
#
# dopewars is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with dopewars.  If not, see <https://www.gnu.org/licenses/>.
module ApplicationHelper
  @@hmac_secret = nil
  def hmac_secret
    unless @hmac_secret
      @hmac_secret = Base64.decode64 File.read(".token_key")
    end
    @hmac_secret
  end
  HASH_ALPHABET = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz".freeze
  class Randomizer
    def initialize
      @prng = Random.new(Time.now.to_i)
    end
    # Returns a random integer.
    # @param max [Integer] Maximum value, exclusive.
    # @return [Integer]
    def random_number(max)
      @prng.rand(max)
    end
  end
  @@randomizer = Randomizer.new
  # Returns a random integer.
  # @param min [Integer] Mininum value, inclusive.
  # @param max [Integer] Maximum value, inclusive.
  # @return [Integer]
  def random_number(min, max)
    @@randomizer.random_number(max - min + 1) + min
  end
  # Creates a new hash
  # @param opts [Hash]
  # @return [String]
  def generate_hash(opts = {})
    len = opts.fetch(:length, 32)
    pool = opts.fetch(:pool, HASH_ALPHABET)
    sb = StringIO.new
    len.times do
      sb << pool[@@randomizer.random_number(pool.length)]
    end
    sb.string
  end
  def hexdigest(*values)
    sha = Digest::SHA256.new
    values.each { |v| sha.update v }
    sha.hexdigest
  end
  def validate_hash(hash)
    halt 422, {'message' => 'wrong hash'}.to_json if hash.length != 32
    halt 422, {'message' => 'wrong hash'}.to_json unless hash.match(/^[0-9A-Za-z]+$/)
  end
  # @return [Hash]
  def parse_json_body
    request.body.rewind
    begin
      JSON.parse(request.body.read)
    rescue JSON::ParserError => e
      logger.error e.message
      e.backtrace.each { |t| logger.error t }
      halt 422, {'message' => 'wrong content'}.to_json
    end
  end
  # @param obj [Object]
  # @param fields [Array<Symbol>]
  # @return [Hash]
  def convert_to_hash(obj, fields)
    new_hash = {}
    fields.each { |f| new_hash[f.to_s] = obj[f] }
    new_hash
  end
  # Formats a given credit.
  # @param cr [Integer, String]
  # @return [String]
  def format_credit(cr)
    cr = cr.to_s
    reader = StringIO.new cr
    sb = StringIO.new
    n = 0
    while n < cr.length
      c = (cr.length - n) % 3
      if c == 0
        sb << "," unless sb.length == 0
        sb << reader.read(3)
        n += 3
      else
        sb << reader.read(c)
        n += c
      end
    end
    sb.string
  end
  # Formats a given amount of elapsed time in seconds.
  # @param secs [Integer]
  # @return [String]
  def format_elapsed_time(secs)
    value = secs
    unit = "second"
    if secs >= 86_400
      value = secs / 86_400
      unit = "day"
    elsif secs >= 3_600
      value = secs / 3_600
      unit = "hour"
    elsif secs >= 60
      value = secs / 60
      unit = "minute"
    end
    unit = "#{unit}s" if value > 1
    "#{value} #{unit}"
  end
  DrugSpec = Struct.new("DrugSpec", :kind, :name, :min, :max, :cheap, :expensive)
  LocationSpec = Struct.new("LocationSpec", :kind, :name, :bank, :debt, :hire, :min_drugs, :max_drugs)
  @@drug_specs = nil
  # Returns the drug specs.
  # @return [Array]
  def drug_specs
    unless @@drug_specs
      @@drug_specs = []
      h = YAML.load_file("app/helpers/drugs.yaml")
      h["drugs"].each_with_index do |d, i|
        @@drug_specs << DrugSpec.new(i + 1, d["name"], d["min"].to_i, d["max"].to_i, d["cheap"], d["expensive"])
      end
    end
    @@drug_specs
  end
  # Returns drug spec selection.
  # @param opts [Hash]
  # @option opts [Boolean] :cheap_only
  # @option opts [Boolean] :expensive_only
  # @option opts [Boolean] :cheap_and_expensive_only
  # @return [Array]
  def select_drug_specs(opts = {})
    if opts.fetch(:cheap_and_expensive_only, false)
      drug_specs.select { |s| s.cheap || s.expensive }
    elsif opts.fetch(:cheap_only, false)
      drug_specs.select { |s| s.cheap }
    elsif opts.fetch(:expensive_only, false)
      drug_specs.select { |s| s.expensive }
    else
      drug_specs
    end
  end
  @@location_specs = nil
  # Returns the location specs.
  # @return [Array]
  def location_specs
    unless @@location_specs
      @@location_specs = []
      h = YAML.load_file("app/helpers/locations.yaml")
      h["locations"].each_with_index do |d, i|
        @@location_specs << LocationSpec.new(i + 1, d["name"], !!d["bank"], !!d["debt"], !!d["hire"], d["min_drugs"].to_i, d["max_drugs"].to_i)
      end
    end
    @@location_specs
  end
end
