# Copyright (C) 2020 Thomas Baron
#
# This file is part of dopewars.
#
# dopewars is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3 of the License.
#
# dopewars is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with dopewars.  If not, see <https://www.gnu.org/licenses/>.
module PlayerHelper
  def validate_slug(slug)
    raise StandardError, "slug must contain at least 4 characters" if slug.length < 4
    raise StandardError, "slug must contain at most 30 characters" if slug.length > 30
    raise StandardError, "slug must contain acceptable characters" unless slug.match(/^[0-9A-Za-z!@#$%&_?-]+$/)
  end
  # Creates a new player.
  # @param name [String]
  # @param password [String]
  # @return [Player]
  def create_player(name, password)
    validate_slug name
    validate_slug password
    player = Player.where("name = '#{name}'").take
    raise StandardError, 'player already exists' if player
    salt = generate_hash :length => 32
    pass = hexdigest salt, password
    Player.create({:public_id => generate_hash, :name => name, :pass => pass, :salt => salt})
  end
  # Authenticates a player.
  # @param name [String]
  # @param password [String]
  # @return [Player]
  def authenticate_player(name, password)
    validate_slug name
    validate_slug password
    player = Player.where("name = '#{name}'").take
    raise StandardError, 'player does not exist' unless player
    pass = hexdigest player.salt, password
    raise "authentication failed" if player.pass != pass
    player
  end
  # @param opts [Hash]
  # @option opts [String] :public_id
  # @return [Player]
  def fetch_player(opts = {})
    player = nil
    if opts.has_key? :public_id
      player = Player.where("public_id = '#{opts[:public_id]}'").take
    end
    raise StandardError, 'no such player' unless player
    player
  end
  # @param ip [String]
  # @return [Integer]
  def incr_player_creation_counter(ip)
    raise StandardError, "ip must contain acceptable characters" unless ip.match(/^[0-9]{1,3}[.][0-9]{1,3}[.][0-9]{1,3}[.][0-9]{1,3}$/)
    source_ip = SourceIP.where(['address = ?', ip]).take
    if source_ip
      source_ip.player_creation_count += 1
      source_ip.save
    else
      source_ip = SourceIP.create({address: ip, player_creation_count: 1})
    end
    source_ip.player_creation_count
  end
end
