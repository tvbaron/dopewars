# Copyright (C) 2020 Thomas Baron
#
# This file is part of dopewars.
#
# dopewars is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3 of the License.
#
# dopewars is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with dopewars.  If not, see <https://www.gnu.org/licenses/>.
module Api
  class PlayerController < ApplicationController
    helpers PlayerHelper
    helpers SessionHelper
    post '/create' do
      content_type :json
      halt 422, {'message' => 'missing player_name'}.to_json unless params.has_key? 'player_name'
      halt 422, {'message' => 'missing player_password'}.to_json unless params.has_key? 'player_password'
      player_name = params['player_name']
      player_password = params['player_password']
      begin
        count = incr_player_creation_counter request.ip
        raise StandardError, 'come back later' if count > 10
        player = create_player(player_name, player_password)
        {:hash => player.public_id, :name => player.name}.to_json
      rescue StandardError => e
        logger.error e.message
        e.backtrace.each { |t| logger.error t }
        halt 422, {'message' => e.message}.to_json
      end
    end
    post '/authenticate' do
      content_type :json
      halt 422, {'message' => 'missing player_name'}.to_json unless params.has_key? 'player_name'
      halt 422, {'message' => 'missing player_password'}.to_json unless params.has_key? 'player_password'
      player_name = params['player_name']
      player_password = params['player_password']
      begin
        player = authenticate_player(player_name, player_password)
        token = create_session_token player
        {:token => token}.to_json
      rescue StandardError => e
        logger.error e.message
        e.backtrace.each { |t| logger.error t }
        halt 401, {'message' => e.message}.to_json
      end
    end
    get '/profile' do
      content_type :json
      player = authenticate_session
      {:hash => player.public_id, :name => player.name}.to_json
    end
  end
end
