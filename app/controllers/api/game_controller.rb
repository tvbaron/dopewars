# Copyright (C) 2020 Thomas Baron
#
# This file is part of dopewars.
#
# dopewars is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3 of the License.
#
# dopewars is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with dopewars.  If not, see <https://www.gnu.org/licenses/>.
module Api
  class GameController < ApplicationController
    helpers PlayerHelper
    helpers GameHelper
    helpers SessionHelper
    get '/' do
      content_type :json
      select = 'hightest_score'
      if params.has_key? 'select'
        select = params['select']
      end
      limit = 10
      if params.has_key? 'limit'
        begin
          limit = Integer(params['limit'])
        rescue StandardError => e
          logger.error e.message
          e.backtrace.each { |t| logger.error t }
          halt 422, {'message' => e.message}.to_json
        end
      end
      res = {}
      if select == 'hightest_score'
        now = Time.now.to_i
        games = []
        Game.where('standard != 0 AND active = 0 AND score > 0').order('score DESC').limit(limit).each_with_index do |g, i|
          player = g.player
          ranking = i + 1
          games << {:hash => g.public_id, :score => g.score, :ranking => ranking, :player_name => player.name, :elapsed_time => format_elapsed_time(now - g.updated_at)}
        end
        res[:games] = games
      elsif select == 'recent'
        player = authenticate_session
        res[:player_hash] = player.public_id
        res[:player_name] = player.name
        now = Time.now.to_i
        games = []
        Game.joins(:player).where("player.id = #{player.id}").order('game.updated_at DESC').limit(limit).each_with_index do |g|
          standard = (g.standard != 0)
          config = nil
          unless standard
            config = JSON.parse(g.config)
          end
          if g.active != 0
            play = g.play
            games << {:hash => g.public_id, :active => true, :standard => standard, :config => config, :elapsed_time => format_elapsed_time(now - g.updated_at), :turn => play.turn}
          else
            games << {:hash => g.public_id, :active => false, :standard => standard, :config => config, :elapsed_time => format_elapsed_time(now - g.updated_at), :score => g.score}
          end
        end
        res[:games] = games
      else
        halt 422, {'message' => 'wrong select'}.to_json
      end
      res.to_json
    end
    get '/config' do
      res = {}
      res[:turn_limit] = GameHelper::TURN_LIMIT
      res[:drugs] = []
      drug_specs.each do |s|
        res[:drugs] << {:name => s.name, :min_value => s.min, :max_value => s.max, :cheap => s.cheap, :expensive => s.expensive}
      end
      res[:locations] = []
      location_specs.each do |s|
        res[:locations] << {:name => s.name, :bank => s.bank, :debt => s.debt, :hire => s.hire, :min_drugs => s.min_drugs, :max_drugs => s.max_drugs}
      end
      res.to_json
    end
    post '/create' do
      content_type :json
      player = authenticate_session
      begin
        creation_count = eval_game_creation_count player.id, 600
        raise StandardError, 'come back later' if creation_count >= 10
        options = {}
        if params['custom'] == '1'
          # custom game configuration
          options[:custom] = true
          options[:turn_limit] = Integer(params.fetch('turn_limit')) if params.has_key? 'turn_limit'
          options[:cash] = Integer(params.fetch('cash')) if params.has_key? 'cash'
          options[:debt] = Integer(params.fetch('debt')) if params.has_key? 'debt'
          options[:sidekicks] = Integer(params.fetch('sidekicks')) if params.has_key? 'sidekicks'
          options[:sidekick_comp] = Integer(params.fetch('sidekick_comp')) if params.has_key? 'sidekick_comp'
          options[:sidekick_profit_sharing] = Integer(params.fetch('sidekick_profit_sharing')) if params.has_key? 'sidekick_profit_sharing'
          options[:bank_interest] = Integer(params.fetch('bank_interest')) if params.has_key? 'bank_interest'
          options[:debt_interest] = Integer(params.fetch('debt_interest')) if params.has_key? 'debt_interest'
          options[:hire_sidekick] = true if params['hire_sidekick'] == '1'
          options[:offer_every_drug] = true if params['offer_every_drug'] == '1'
          options[:offer_cheap_drug] = true if params['offer_cheap_drug'] == '1'
          options[:offer_expensive_drug] = true if params['offer_expensive_drug'] == '1'
        end
        game = create_game(player.id, options)
        inventory(game).to_json
      rescue StandardError => e
        logger.error e.message
        e.backtrace.each { |t| logger.error t }
        halt 422, {'message' => e.message}.to_json
      end
    end
    get '/:hash/inventory' do
      content_type :json
      halt 422, {'message' => 'missing game hash'}.to_json unless params.has_key? 'hash'
      validate_hash params['hash']
      game = Game.where(['public_id = ?', params['hash']]).take
      halt 404, {'message' => 'no such game'}.to_json unless game
      if game.active != 0
        player = authenticate_session
        halt 422, {'message' => 'not owner'}.to_json unless game.player.id == player.id
      end
      inventory(game).to_json
    end
    post '/:hash/jet' do
      content_type :json
      player = authenticate_session
      halt 422, {'message' => 'missing game hash'}.to_json unless params.has_key? 'hash'
      validate_hash params['hash']
      halt 422, {'message' => 'missing location'}.to_json unless params.has_key? 'location_name'
      game = Game.where(['public_id = ?', params['hash']]).take
      halt 404, {'message' => 'no such game'}.to_json unless game
      halt 422, {'message' => 'game is not active'}.to_json unless game.active != 0
      halt 422, {'message' => 'not owner'}.to_json unless game.player.id == player.id
      location_spec = location_specs.find { |loc| loc.name == params['location_name'] }
      halt 422, {'message' => 'no such location'}.to_json unless location_spec
      begin
        config = nil
        turn_limit = GameHelper::TURN_LIMIT
        if (game.standard == 0) && game.config
          config = JSON.parse(game.config)
          turn_limit = config['turn_limit']
        end
        play = game.play
        if play.turn < turn_limit
          create_turn(game, play, config, location_spec)
        else
          terminate_game(game, play)
        end
      rescue StandardError => e
        logger.error e.message
        e.backtrace.each { |t| logger.error t }
        halt 422, {'message' => e.message}.to_json
      end
      inventory(game).to_json
    end
    post '/:hash/put' do
      content_type :json
      player = authenticate_session
      halt 422, {'message' => 'missing game hash'}.to_json unless params.has_key? 'hash'
      validate_hash params['hash']
      halt 422, {'message' => 'amount'}.to_json unless params.has_key? 'cash'
      game = Game.where(['public_id = ?', params['hash']]).take
      halt 404, {'message' => 'no such game'}.to_json unless game
      halt 422, {'message' => 'game is not active'}.to_json unless game.active != 0
      halt 422, {'message' => 'not owner'}.to_json unless game.player.id == player.id
      begin
        cash = Integer(params['cash'])
        halt 422, {'message' => 'wrong cash'}.to_json unless cash > 0
        make_deposit(game, cash)
      rescue StandardError => e
        logger.error e.message
        e.backtrace.each { |t| logger.error t }
        halt 422, {'message' => e.message}.to_json
      end
      inventory(game).to_json
    end
    post '/:hash/take' do
      content_type :json
      player = authenticate_session
      halt 422, {'message' => 'missing game hash'}.to_json unless params.has_key? 'hash'
      validate_hash params['hash']
      halt 422, {'message' => 'amount'}.to_json unless params.has_key? 'cash'
      game = Game.where(['public_id = ?', params['hash']]).take
      halt 404, {'message' => 'no such game'}.to_json unless game
      halt 422, {'message' => 'game is not active'}.to_json unless game.active != 0
      halt 422, {'message' => 'not owner'}.to_json unless game.player.id == player.id
      begin
        cash = Integer(params['cash'])
        halt 422, {'message' => 'wrong cash'}.to_json unless cash > 0
        make_withdraw(game, cash)
      rescue StandardError => e
        logger.error e.message
        e.backtrace.each { |t| logger.error t }
        halt 422, {'message' => e.message}.to_json
      end
      inventory(game).to_json
    end
    post '/:hash/pay' do
      content_type :json
      player = authenticate_session
      halt 422, {'message' => 'missing game hash'}.to_json unless params.has_key? 'hash'
      validate_hash params['hash']
      halt 422, {'message' => 'amount'}.to_json unless params.has_key? 'cash'
      game = Game.where(['public_id = ?', params['hash']]).take
      halt 404, {'message' => 'no such game'}.to_json unless game
      halt 422, {'message' => 'game is not active'}.to_json unless game.active != 0
      halt 422, {'message' => 'not owner'}.to_json unless game.player.id == player.id
      begin
        cash = Integer(params['cash'])
        halt 422, {'message' => 'wrong cash'}.to_json unless cash > 0
        pay_debt(game, cash)
      rescue StandardError => e
        logger.error e.message
        e.backtrace.each { |t| logger.error t }
        halt 422, {'message' => e.message}.to_json
      end
      inventory(game).to_json
    end
    post '/:hash/hire' do
      content_type :json
      player = authenticate_session
      halt 422, {'message' => 'missing game hash'}.to_json unless params.has_key? 'hash'
      validate_hash params['hash']
      game = Game.where(['public_id = ?', params['hash']]).take
      halt 404, {'message' => 'no such game'}.to_json unless game
      halt 422, {'message' => 'game is not active'}.to_json unless game.active != 0
      halt 422, {'message' => 'not owner'}.to_json unless game.player.id == player.id
      begin
        hire_sidekick(game)
      rescue StandardError => e
        logger.error e.message
        e.backtrace.each { |t| logger.error t }
        halt 422, {'message' => e.message}.to_json
      end
      inventory(game).to_json
    end
    post '/:hash/buy' do
      content_type :json
      player = authenticate_session
      halt 422, {'message' => 'missing game hash'}.to_json unless params.has_key? 'hash'
      validate_hash params['hash']
      halt 422, {'message' => 'missing drug name'}.to_json unless params.has_key? 'drug_name'
      halt 422, {'message' => 'missing quantity'}.to_json unless params.has_key? 'quantity'
      game = Game.where(['public_id = ?', params['hash']]).take
      halt 404, {'message' => 'no such game'}.to_json unless game
      halt 422, {'message' => 'game is not active'}.to_json unless game.active != 0
      halt 422, {'message' => 'not owner'}.to_json unless game.player.id == player.id
      drug_name = params['drug_name']
      begin
        quantity = Integer(params['quantity'])
        halt 422, {'message' => 'wrong quantity'}.to_json unless quantity > 0
        buy_drug(game, drug_name, quantity)
      rescue StandardError => e
        logger.error e.message
        e.backtrace.each { |t| logger.error t }
        halt 422, {'message' => e.message}.to_json
      end
      inventory(game).to_json
    end
    post '/:hash/sell' do
      content_type :json
      player = authenticate_session
      halt 422, {'message' => 'missing game hash'}.to_json unless params.has_key? 'hash'
      validate_hash params['hash']
      halt 422, {'message' => 'missing drug name'}.to_json unless params.has_key? 'drug_name'
      halt 422, {'message' => 'missing quantity'}.to_json unless params.has_key? 'quantity'
      game = Game.where(['public_id = ?', params['hash']]).take
      halt 404, {'message' => 'no such game'}.to_json unless game
      halt 422, {'message' => 'game is not active'}.to_json unless game.active != 0
      halt 422, {'message' => 'not owner'}.to_json unless game.player.id == player.id
      drug_name = params['drug_name']
      begin
        quantity = Integer(params['quantity'])
        halt 422, {'message' => 'wrong quantity'}.to_json unless quantity > 0
        sell_drug(game, drug_name, quantity)
      rescue StandardError => e
        logger.error e.message
        e.backtrace.each { |t| logger.error t }
        halt 422, {'message' => e.message}.to_json
      end
      inventory(game).to_json
    end
    post '/:hash/drop' do
      content_type :json
      player = authenticate_session
      halt 422, {'message' => 'missing game hash'}.to_json unless params.has_key? 'hash'
      validate_hash params['hash']
      halt 422, {'message' => 'missing drug name'}.to_json unless params.has_key? 'drug_name'
      halt 422, {'message' => 'missing quantity'}.to_json unless params.has_key? 'quantity'
      game = Game.where(['public_id = ?', params['hash']]).take
      halt 404, {'message' => 'no such game'}.to_json unless game
      halt 422, {'message' => 'game is not active'}.to_json unless game.active != 0
      halt 422, {'message' => 'not owner'}.to_json unless game.player.id == player.id
      drug_name = params['drug_name']
      begin
        quantity = Integer(params['quantity'])
        halt 422, {'message' => 'wrong quantity'}.to_json unless quantity > 0
        drop_drug(game, drug_name, quantity)
      rescue StandardError => e
        logger.error e.message
        e.backtrace.each { |t| logger.error t }
        halt 422, {'message' => e.message}.to_json
      end
      inventory(game).to_json
    end
  end
end
