# Copyright (C) 2020 Thomas Baron
#
# This file is part of dopewars.
#
# dopewars is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3 of the License.
#
# dopewars is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with dopewars.  If not, see <https://www.gnu.org/licenses/>.
class ApplicationController < Sinatra::Base
  helpers Sinatra::ContentFor
  helpers ApplicationHelper
  set :public_folder, 'public'
  set :views, 'app/views'
  # environment configuration
  set :lock, true
  configure :development do
    set :logging, true
  end
  configure :production do
    set :logging, false
    set :dump_errors, false
    set :raise_errors, false
    set :show_exceptions, false
  end
end
