# Copyright (C) 2020 Thomas Baron
#
# This file is part of dopewars.
#
# dopewars is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3 of the License.
#
# dopewars is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with dopewars.  If not, see <https://www.gnu.org/licenses/>.
module Web
  class HomeController < ApplicationController
    get '/' do
      settings = {}
      settings[:base_url] = ENV['BASE_URL']
      settings[:app_name] = ENV['APP_NAME']
      now = Time.now.to_i
      games = []
      Game.where('standard != 0 AND active = 0 AND score > 0').order('score DESC').limit(20).each_with_index do |g, i|
        player = g.player
        ranking = i + 1
        games << {hash: g.public_id, score: g.score, ranking: ranking, player_name: player.name, elapsed_time: format_elapsed_time(now - g.updated_at)}
      end
      erb :index, locals: {settings: settings, games: games}
    end
    get '/log_in' do
      settings = {}
      settings[:base_url] = ENV['BASE_URL']
      settings[:app_name] = ENV['APP_NAME']
      erb :log_in, locals: {settings: settings}
    end
    get '/sign_up' do
      settings = {}
      settings[:base_url] = ENV['BASE_URL']
      settings[:app_name] = ENV['APP_NAME']
      erb :sign_up, locals: {settings: settings}
    end
    get '/about' do
      settings = {}
      settings[:base_url] = ENV['BASE_URL']
      settings[:app_name] = ENV['APP_NAME']
      config = {}
      config[:turn_limit] = GameHelper::TURN_LIMIT
      config[:drugs] = []
      drug_specs.each do |s|
        config[:drugs] << {name: s.name, min_value: s.min, max_value: s.max, cheap: s.cheap, expensive: s.expensive}
      end
      config[:locations] = []
      location_specs.each do |s|
        config[:locations] << {name: s.name, bank: s.bank, debt: s.debt, hire: s.hire, min_drugs: s.min_drugs, max_drugs: s.max_drugs}
      end
      erb :about, locals: {settings: settings, config: config}
    end
    get '/wall' do
      settings = {}
      settings[:base_url] = ENV['BASE_URL']
      settings[:app_name] = ENV['APP_NAME']
      now = Time.now.to_i
      games = []
      Game.where('standard != 0 AND active = 0 AND score > 0').order('score DESC').limit(200).each_with_index do |g, i|
        player = g.player
        ranking = i + 1
        games << {hash: g.public_id, score: g.score, ranking: ranking, player_name: player.name, elapsed_time: format_elapsed_time(now - g.updated_at)}
      end
      erb :wall, locals: {settings: settings, games: games}
    end
  end
end
