# Copyright (C) 2020 Thomas Baron
#
# This file is part of dopewars.
#
# dopewars is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3 of the License.
#
# dopewars is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with dopewars.  If not, see <https://www.gnu.org/licenses/>.
module Web
  class GameController < ApplicationController
    get '/:hash' do
      validate_hash params['hash']
      game = Game.where(['public_id = ?', params['hash']]).take
      halt 404, "no such game" unless game
      settings = {}
      settings[:base_url] = ENV['BASE_URL']
      if game.active != 0
        erb :game, :locals => {:settings => settings, :game => game}
      else
        erb :score, :locals => {:settings => settings, :game => game}
      end
    end
  end
end
